package de.hsharz.ai.st.sequence;

public class BMI {

	public double bmi(double height, double weight) {
		if (height <= 0) {
			return -1;
		}
		return weight / (height * height);
	}

}
