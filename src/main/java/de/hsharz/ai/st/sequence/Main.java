package de.hsharz.ai.st.sequence;

public class Main {

	public static void main(String[] args) {
		if (args.length == 2) {
			int a = Integer.parseInt(args[0]);
			int b = Integer.parseInt(args[1]);
			int sum = add(a, b);
			System.out.println("Summe = " + sum);
		}
	}

	public static int add(int a, int b) {
		return a + b;
	}

}
