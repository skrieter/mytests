package de.hsharz.ai.st.sequence;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileReader {

	public static String readFile(Path path) {
		try {
			final byte[] readAllBytes = Files.readAllBytes(path);
			return new String(readAllBytes);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
