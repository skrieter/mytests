package de.hsharz.ai.st.sequence;

import static org.junit.Assert.assertEquals;

public class BMITest {

	@org.junit.Test
	public void testCase01() {
		assertEquals(1, new BMI().bmi(1, 1), 0); 
	}
	
	@org.junit.Test
	public void testCase02() {
		assertEquals(-1, new BMI().bmi(-1, -1), 0); 
	}
	
	@org.junit.Test
	public void testCase03() {
		assertEquals(20, new BMI().bmi(2, 80), 0); 
	}
	
	@org.junit.Test
	public void testCase04() {
		assertEquals(25, new BMI().bmi(1.8, 81), 0); 
	}

}
