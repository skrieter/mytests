package de.hsharz.ai.st.sequence;

import static org.junit.Assert.assertEquals;

import java.nio.file.Path;
import java.nio.file.Paths;

public class FileReaderTest {

	private final Path testFiles = Paths.get("src/test/testFiles");

	@org.junit.Test
	public void testCase01() {
		final Path path = testFiles.resolve("testFile01.txt");
		assertEquals("Hallo Welt!", FileReader.readFile(path)); 
	}
	@org.junit.Test
	public void testCase02() {
		final Path path = testFiles.resolve("testFile02.txt");
		assertEquals("Hello World!\r\n42", FileReader.readFile(path)); 
	}
	
}
