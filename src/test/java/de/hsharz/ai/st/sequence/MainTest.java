package de.hsharz.ai.st.sequence;

import static org.junit.Assert.assertEquals;

public class MainTest {

	@org.junit.Test
	public void testCase01() {
		int result = Main.add(2, 4);
		assertEquals(6, result);
	}

	@org.junit.Test
	public void testCase02() {
		int result = Main.add(1, 2);
		assertEquals(3, result);
	}

	@org.junit.Test
	public void testCase03() {
		Main.main(new String[] { "1", "4" });
	}

	@org.junit.Test
	public void testCase04() {
		Main.main(new String[] { "4" });
	}

}
